About
--------------------------------
Simple Apps to play with the capabilities of [cesium](http://cesium.agi.com/)

License
---------------------------------
Cesium is licensed under [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.html) and is free for both commercial and non-commercial use.
At present I don't know any appropriate license, but you are free to use it for any purpose you see fit. It would be nice of you if you would like
to give me some credit for this :) .

Installing
----------------------------------
First get a web server of your choice. Any server of serving request should do. I am using [WAMP](http://www.wampserver.com/en/) by the way.
Then, in its document root (www folder inside wamp), clone this repo.
```bash
git clone https://github.com/ayudhDas/cesiumApps.git
```

Start the server and point the browser to (http://localhost:port/cesiumApps)
You are all set. :)

Troubleshooting
-----------------------------------
You need a HTML5 capable browser. I recommend [google chrome 24+](http://www.google.com/chrome/) or [mozilla firefox 19+](http://www.mozilla.org/en-US/firefox/new/).
Plus check if your browser can handle webgl correctly or not. You may find help here
<ul>
	<li>[webgl site](http://get.webgl.org/)</li>
	<li>[webgl report](http://webglreport.com/)</li>
</ul>

If you still have troubles running cesium, ask the nice guys [here](https://groups.google.com/d/forum/cesium-dev)

Impressed. Wanna do some more
-----------------------------------
So you want some more. Why don't you check out https://github.com/AnalyticalGraphicsInc/cesium ?
Just fork the repo and start hacking. Read [this](https://github.com/AnalyticalGraphicsInc/cesium/wiki/Contributor%27s-Guide) to get started.
If you want to help in the development of cesium, make sure you read [this](https://github.com/AnalyticalGraphicsInc/cesium/blob/master/CONTRIBUTING.md) first.